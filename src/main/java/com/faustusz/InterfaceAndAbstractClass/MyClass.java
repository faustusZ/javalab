package com.faustusz.InterfaceAndAbstractClass;

public class MyClass extends MyAbsClass {

    @Override
    public void foo1() {
        System.out.println("" + MyClass.FIELD);
    }

    @Override
    public void foo2() {
        System.out.println("" + (MyClass.FIELD + MyClass.FIELD));
    }
}
