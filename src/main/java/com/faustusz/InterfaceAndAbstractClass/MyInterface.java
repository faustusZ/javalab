package com.faustusz.InterfaceAndAbstractClass;

public interface MyInterface {

    /*
    * 接口, Interface
    * 在接口中的方法(method)全部会自动地被设定为 public, 所以不用加任何 modifier
    * 在接口中的字段(field)全部会被设定为 public static, 所以不用加任何 modifier
    */

    int FIELD = 1;

    void foo1();

    void foo2();
}
