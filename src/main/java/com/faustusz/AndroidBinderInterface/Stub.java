package com.faustusz.AndroidBinderInterface;

public abstract class Stub extends Binder implements MyInterface {
    public static MyInterface asInterface(IBinder obj) {
        return new Proxy(obj);
    }
}
