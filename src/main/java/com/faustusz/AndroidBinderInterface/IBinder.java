package com.faustusz.AndroidBinderInterface;

public interface IBinder {
    boolean transact(int code, int data, int reply, int flags);
}
