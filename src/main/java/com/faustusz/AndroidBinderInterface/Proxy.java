package com.faustusz.AndroidBinderInterface;

public class Proxy implements MyInterface {

    private IBinder mRemote;

    Proxy(IBinder remote) {
        mRemote = remote;
    }

    @Override
    public IBinder asBinder() {
        return mRemote;
    }

    @Override
    public int basicTypes() {
        mRemote.transact(0, 0, 0, 0);
        return 0;
    }
}
