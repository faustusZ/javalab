package com.faustusz.AndroidBinderInterface;

public class Binder implements IBinder {
    protected boolean onTransact(int code, int data, int reply, int flags) {
        return true;
    }

    @Override
    public final boolean transact(int code, int data, int reply, int flags) {
        boolean r = onTransact(code, data, reply, flags);
        return r;
    }
}
