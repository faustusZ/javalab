package com.faustusz.TryCatchFinally;

import static com.faustusz.TryCatchFinally.TryCatchFinally.Trace.TRACE_TAG_ACTIVITY_MANAGER;

public class TryCatchFinally {


    ResolveInfo resolveIntent(Intent intent, String resolvedType, int userId, int flags,
                              int filterCallingUid) {
        try {
            Trace.traceBegin(TRACE_TAG_ACTIVITY_MANAGER, "resolveIntent");
            int modifiedFlags = flags
                    | PackageManager.MATCH_DEFAULT_ONLY | ActivityManagerService.STOCK_PM_FLAGS;
            if (intent.isWebIntent()
                    || (intent.getFlags() & Intent.FLAG_ACTIVITY_MATCH_EXTERNAL) != 0) {
                modifiedFlags |= PackageManager.MATCH_INSTANT;
            }

            // In order to allow cross-profile lookup, we clear the calling identity here.
            // Note the binder identity won't affect the result, but filterCallingUid will.

            // Cross-user/profile call check are done at the entry points
            // (e.g. AMS.startActivityAsUser).
            final long token = Binder.clearCallingIdentity();
            try {
                return mService.getPackageManagerInternalLocked().resolveIntent(
                        intent, resolvedType, modifiedFlags, userId, true, filterCallingUid);
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        } finally {
            Trace.traceEnd(TRACE_TAG_ACTIVITY_MANAGER);
        }
    }












    public static class ResolveInfo {}

    public static class Intent {}

    public static class Trace {
        public final static int TRACE_TAG_ACTIVITY_MANAGER = 0;
        public static void traceBegin(int i, String s) {}
        public static void traceEnd(int i) {}
    }

    public static class Binder {}
}
