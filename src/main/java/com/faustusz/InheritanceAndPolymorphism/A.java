package com.faustusz.InheritanceAndPolymorphism;

public class A {
    private static final String TAG = "A";

    public void foo1() {
        System.out.println("I'm foo1() in " + TAG + ".");
        foo2();
    }

    public void foo2() {
        System.out.println("I'm foo2() in " + TAG + ".");
    }

    protected void foo3(String arg) {

    }
}
