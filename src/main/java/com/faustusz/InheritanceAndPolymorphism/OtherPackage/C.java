package com.faustusz.InheritanceAndPolymorphism.OtherPackage;

import com.faustusz.InheritanceAndPolymorphism.A;
import com.faustusz.InheritanceAndPolymorphism.B;

public class C extends B {

    private static final String TAG = "C";

    @Override
    public void foo2() {
        System.out.println("I'm foo2() in " + TAG + ".");
    }

}
