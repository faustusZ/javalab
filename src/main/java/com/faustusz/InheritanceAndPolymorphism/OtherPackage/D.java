package com.faustusz.InheritanceAndPolymorphism.OtherPackage;

import com.faustusz.InheritanceAndPolymorphism.A;

public class D extends C {

    private static final String TAG = "D";

    @Override
    public void foo3(String s) {
        super.foo3(s);
    }

    public static void main(String[] args) {
        A a;
        a = new C();
        a.foo1();

        D d = new D();
        d.foo3("s");
    }
}
