package com.faustusz.InheritanceAndPolymorphism;

public class B extends A {

    private static final String TAG = "B";

    public void fooB() {
        System.out.println("I'm fooB() in " + TAG + ".");
    }
}
