package com.faustusz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class StringOperation {

    private static final Logger LOGGER = LogManager.getLogger(StringOperation.class);

    private static void testSubString() {
        String original
                = "com.oppo.ovoicemanager.middleware.OVoiceManagerStateMachine$DspDetectingState@7d40d1b";
        String modified = original.substring(original.indexOf("$") + 1, original.indexOf("@"));
        LOGGER.info(modified);
    }

    private static void testSplit() {
        String sen = "a:b:c";
        LOGGER.info(sen);
        String[] words = sen.split(":");
        LOGGER.info(Arrays.toString(words));
    }

    public static void stringLength(String s) {
        LOGGER.info("" + s.length());
    }
}
