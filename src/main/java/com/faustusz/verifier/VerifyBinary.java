package com.faustusz.verifier;

import com.faustusz.Binary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerifyBinary {

    private static final Logger LOGGER = LogManager.getLogger(VerifyBinary.class);

    public static void main(String[] args) {
        LOGGER.info(Binary.get((long) -123));
        LOGGER.info(Binary.get(-123));
        LOGGER.info(Binary.get((short) -123));
        LOGGER.info(Binary.get((byte) -123));

        LOGGER.info(Binary.toLong("1111111111111111111111111111111111111111111111111111111110000101"));
        LOGGER.info(Binary.toInteger("11111111111111111111111110000101"));
        LOGGER.info(Binary.toShort("1111111110000101"));
        LOGGER.info(Binary.toByte("10000101"));
    }
}
