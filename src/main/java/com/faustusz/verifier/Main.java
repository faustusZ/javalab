package com.faustusz.verifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        // write your code here
        testArrayList();
        testInterpolate();
    }

    private static void testArrayList() {
        List<Integer> result = new ArrayList<>();
        result.add(Integer.parseInt("012"));
        result.add(Integer.parseInt("012"));
        LOGGER.info(result.toString());
    }

    private static void testInterpolate() {

        byte[] data = new byte[5];
        LOGGER.info(Arrays.toString(data));

        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) (i + 1);
        }
        LOGGER.info(Arrays.toString(data));

        byte[] newData = interpolateRefs(data);
        LOGGER.info(Arrays.toString(newData));

    }

    private static byte[] interpolateRefs(final byte[] data) {
        byte[] newData = new byte[(data.length + data.length % 2) * 2];
        int i = 0;
        while (i < data.length - 1) {
            newData[i * 2] = data[i];
            newData[i * 2 + 1] = data[i + 1];
            i += 2;
        }
        if (i == data.length - 1) {
            newData[i * 2] = data[i];
        }
        return newData;
    }
}
