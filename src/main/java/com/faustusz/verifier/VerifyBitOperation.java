package com.faustusz.verifier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VerifyBitOperation {

    private static final Logger LOGGER = LogManager.getLogger(VerifyBitOperation.class);

    private static final int CHANNEL_IN_LEFT = 0x4;
    private static final int CHANNEL_IN_RIGHT = 0x8;
    private static final int CHANNEL_IN_FRONT = 0x10;
    private static final int CHANNEL_IN_BACK = 0x20;

    public static void main(String[] args) {

        testBit();

        System.out.println("" + (CHANNEL_IN_LEFT | CHANNEL_IN_RIGHT));
        System.out.println("" + (CHANNEL_IN_LEFT | CHANNEL_IN_RIGHT | CHANNEL_IN_FRONT | CHANNEL_IN_BACK));
    }


    private static void testBit() {
        byte byte1 = -7;
        byte byte2 = 7;
        LOGGER.info("" + (byte1 & 0xFF));
        LOGGER.info("" + Integer.toBinaryString(byte1 & 0xFF));
        LOGGER.info("" + (-7));
        LOGGER.info("" + Integer.toBinaryString(-7));
        LOGGER.info("" + (byte1 << 1));
        LOGGER.info("" + Integer.toBinaryString(byte1 << 1));
        LOGGER.info("" + (byte1 >> 1));
        LOGGER.info("" + Integer.toBinaryString(byte1 >> 1));
        LOGGER.info("" + (byte2 << 1));
        LOGGER.info("" + Integer.toBinaryString(byte2 << 1));
        LOGGER.info("" + (byte2 >> 1));
        LOGGER.info("" + Integer.toBinaryString(byte2 >> 1));
    }
}
