package com.faustusz.verifier;

import com.faustusz.InterfaceAndAbstractClass.MyClass;

public class VerifyMyClass {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.foo1();
        myClass.foo2();
    }
}
