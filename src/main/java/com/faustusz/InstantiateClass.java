package com.faustusz;

public class InstantiateClass {

    /*
     * 1. Class Loading: 虚拟机把Class文件加载到内存
     *   注: Class文件应该是一串二进制的字节流, 无论是来自硬盘, 网络还是什么地方都可以.
     *
     * 1) 加载
     *       (1) 通过一个类的全限定名来获取定义此类的二进制字节流
     *       (2) 将这个字节流代表的静态存储结构转化为方法区的运行时数据结构
     *       (3) 在内存的堆区中生成一个代表这个类的java.lang.Class对象
     * 2) 验证
     * 3) 准备
     * 4) 解析
     * 5) 初始化 <clinit>()
     *       (1) 初始化要做哪些事情
     *           执行对静态域的赋值动作以及静态语句块(static {} 块)中的语句
     *       (2) 初始化的时机
     *           ((1)) 当虚拟机启动时, 虚拟机首先初始化用户指定的要执行的主类
     *           ((2)) 遇到new[实例化对象], getstatic[读取类的静态域], putstatic[设置类的静态域], invokestatic[执行类的静态方法]
     *                 这四条字节码指令时, 如果类没有进行初始化
     *           ((3)) 当初始化一个类的时候, 如果发现父类没有进行过初始化, 则需要先触发其父类的初始化
     *           ((4)) ...
     *           ((5)) ...
     *
     * 2. 对象初始化 <init>()
     *    (1) 初始化所做的事情
     *        ((1)) 调用父类的<init>()方法
     *        ((2)) 对实例域进行初始化
     *        ((3)) 执行对应的构造器内的代码
     *
     *
     */

    private static class MyClass {

        private static final MyClass sMyClass = new MyClass();

        static {
            System.out.println("Static block.");
        }

        MyClass() {
            System.out.println("Constructor.");
        }

        void myMethod() {
            System.out.println("object method.");
        }
    }
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.myMethod();
    }
}
