package com.faustusz;

public class Binary {

    // shift: the log2 of the base to format in (4 for hex, 3 for octal, 1 for binary)
    private final static int shift = 1;
    private final static int mask = 1 << shift - 1;


    /**
     * All possible chars for representing a number as a String
     */
    private final static char[] digits = {
            '0' , '1' , '2' , '3' , '4' , '5' ,
            '6' , '7' , '8' , '9' , 'a' , 'b' ,
            'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
            'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
            'o' , 'p' , 'q' , 'r' , 's' , 't' ,
            'u' , 'v' , 'w' , 'x' , 'y' , 'z'
    };

    public static String get(long l) {
        char[] binStr = new char[Long.SIZE];
        int charPos = Long.SIZE;
        do {
            binStr[--charPos] = Binary.digits[(int) l & mask];
            l >>>= shift; // 向右移位并且左侧补0
        } while (charPos > 0);
        return new String(binStr);
    }

    public static String get(int i) {
        char[] binStr = new char[Integer.SIZE];
        int charPos = Integer.SIZE;
        do {
            binStr[--charPos] = Binary.digits[i & mask];
            i >>>= shift; // 向右移位并且左侧补0
        } while (charPos > 0);
        return new String(binStr);
    }

    public static String get(short s) {
        char[] binStr = new char[Short.SIZE];
        int charPos = Short.SIZE;
        do {
            binStr[--charPos] = Binary.digits[s & mask];
            s >>>= shift; // 向右移位并且左侧补0
        } while (charPos > 0);
        return new String(binStr);
    }

    public static String get(byte b) {
        char[] binStr = new char[Byte.SIZE];
        int charPos = Byte.SIZE;
        do {
            binStr[--charPos] = Binary.digits[b & mask];
            b >>>= shift; // 向右移位并且左侧补0
        } while (charPos > 0);
        return new String(binStr);
    }

    public static long toLong(String str) {
        if (str.length() != Long.SIZE) {
            return 0L;
        }
        if (str.charAt(0) == '0') {
            return toLongUnsigned(str);
        } else if (str.charAt(0) == '1') {
            return -1 - toLongUnsigned(getSupplement(str));
        }
        return 0L;
    }

    public static int toInteger(String str) {
        if (str.length() != Integer.SIZE) {
            return 0;
        }
        if (str.charAt(0) == '0') {
            return toIntegerUnsigned(str);
        } else if (str.charAt(0) == '1') {
            return -1 - toIntegerUnsigned(getSupplement(str));
        }
        return 0;
    }

    public static int toShort(String str) {
        if (str.length() != Short.SIZE) {
            return 0;
        }
        if (str.charAt(0) == '0') {
            return toShortUnsigned(str);
        } else if (str.charAt(0) == '1') {
            return -1 - toShortUnsigned(getSupplement(str));
        }
        return 0;
    }

    public static int toByte(String str) {
        if (str.length() != Byte.SIZE) {
            return 0;
        }
        if (str.charAt(0) == '0') {
            return toByteUnsigned(str);
        } else if (str.charAt(0) == '1') {
            return -1 - toByteUnsigned(getSupplement(str));
        }
        return 0;
    }

    private static long toLongUnsigned(String str) {
        long sum = 0L;
        int sh = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == '1') {
                sum += 1 << sh;
            }
            sh++;
        }
        return sum;
    }

    private static int toIntegerUnsigned(String str) {
        int sum = 0;
        int sh = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == '1') {
                sum += 1 << sh;
            }
            sh++;
        }
        return sum;
    }


    private static short toShortUnsigned(String str) {
        short sum = 0;
        int sh = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == '1') {
                sum += 1 << sh;
            }
            sh++;
        }
        return sum;
    }

    private static int toByteUnsigned(String str) {
        int sum = 0;
        int sh = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == '1') {
                sum += 1 << sh;
            }
            sh++;
        }
        return sum;
    }

    private static String getSupplement(String str) {
        char[] supple = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            supple[i] = getSupplement(str.charAt(i));
        }
        return new String(supple);
    }

    private static char getSupplement(char ch) {
        if (ch == '0') {
            return '1';
        } else if (ch == '1') {
            return '0';
        }
        return '0';
    }
}
